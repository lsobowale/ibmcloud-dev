terraform {
  required_version = ">= 1.3.0"
  required_providers {
    ibm = {
      source  = "IBM-Cloud/ibm"
      version = "~> 1.52.0"
    }
  }
}

# Configure the IBM Provider
provider "ibm" {
  region = var.cloud.region
  zone   = var.cloud.zone
}


# ===========
#  Deploy resources--  use common modules
# ==========
module "scale-ibm" {
  count           = var.deployMode == "new" && length(var.newDeployment) > 1 ? 1 : 0
  source          = "./modules/sycomp-scale-ibm/"
  namePrefix      = var.namePrefix
  zone            = var.cloud.zone
  admin           = var.admin
  userDefinedTags = var.userDefinedTags
  networkConfig   = var.newDeployment.networkConfig
  mgmtConfig      = var.newDeployment.mgmtConfig
  scaleConfig     = var.newDeployment.scaleConfig
  clientConfig    = var.newDeployment.clientConfig
  protocolsConfig = var.newDeployment.protocolsConfig
}

module "scale-ibm-expansion" {
  count           = var.deployMode == "expansion" && length(var.expansionDeployment) > 1 ? 1 : 0
  source          = "./modules/sycomp-scale-ibm-expansion/"
  namePrefix      = var.namePrefix
  zone            = var.cloud.zone
  userDefinedTags = var.userDefinedTags
  expansionId     = var.expansionDeployment.expansionId
  addScaleNodes   = var.expansionDeployment.addScaleNodes
  addClientNodes  = var.expansionDeployment.addClientNodes
  addProtoNodes   = var.expansionDeployment.addProtoNodes
  addDisks        = var.expansionDeployment.addDisks
}
