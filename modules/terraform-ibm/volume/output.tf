#####################################################
# VPC Block Storage
# Copyright 2020 IBM
#####################################################

output "volume_ids" {
  description = "The ID of the volume"
  value       = { for k in ibm_is_volume.volume : k.name => k.id }
}

output "volume_attachments" {
  description = "The volume attachments"
  value       = ibm_is_instance_volume_attachment.attachment
}
