#####################################################
# VPC Block Storage Resource
# Copyright 2020 IBM
#####################################################

resource "ibm_is_volume" "volume" {
  for_each       = var.volumes
  name           = lower("${var.name}-${each.key}")
  resource_group = var.resource_group_id
  zone           = var.zone
  profile        = each.value.profile
  iops           = each.value.profile == "custom" ? try(each.value.iops, null) : null
  capacity       = each.value.sizeInGB
  encryption_key = try(each.value.encryption_key, null)
  tags           = var.tags
}


resource "ibm_is_instance_volume_attachment" "attachment" {
  for_each                           = ibm_is_volume.volume
  instance                           = var.instance_id
  name                               = "${each.value.name}-att"
  volume                             = each.value.id
  delete_volume_on_attachment_delete = false
  delete_volume_on_instance_delete   = false
}