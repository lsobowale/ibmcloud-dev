#####################################################
# VPC Block Storage Parameters
# Copyright 2020 IBM
#####################################################

variable "name" {
  description = "Name of the Volume"
  type        = string
}

variable "zone" {
  description = "Volume Zone"
  type        = string
}

variable "volumes" {
  description = "The volumes to create"
  type        = map(any)
}

variable "instance_id" {
  description = "Instance Id"
  type        = string
}


#####################################################
# Optional Parameters
#####################################################


variable "resource_group_id" {
  description = "Resource group ID"
  type        = string
  default     = null
}

variable "tags" {
  description = "List of Tags for the volume"
  type        = list(string)
  default     = null
}