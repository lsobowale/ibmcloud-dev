locals {
  sshPublicKey     = file(var.admin.sshUserPublicKey)
  sshPrivateKey    = file(var.admin.sshUserPrivateKey)
  primarySubnet    = ibm_is_subnet.subnet["${var.namePrefix}-${var.networkConfig.subnets.primarySubnet.name}"].id
  backendSubnet    = ibm_is_subnet.subnet["${var.namePrefix}-backend-subnet"].id
  secondarySubnets = [for k in var.networkConfig.subnets.secondarySubnets : ibm_is_subnet.subnet["${var.namePrefix}-${k.name}"].id]
  sshKeyIds        = [for k in data.ibm_is_ssh_key.sycomp-keys : k.id]
  mgmtNodes        = [for k in range(var.mgmtConfig.mgmtNodeCount) : "${var.namePrefix}-mgmt${k}"]
  clientNodes      = [for k in range(var.clientConfig.clientNodeCount) : "${var.namePrefix}-client${k}"]
  scaleNodes       = [for k in range(var.scaleConfig.scaleNodeCount) : "${var.namePrefix}-scale${k}"]
  bareMetalNodes   = [for k in range(var.scaleBareMetalConfig.bareMetalCount) : "${var.namePrefix}-scale${k}"]
  scaleVolumes     = merge([for k, v in var.scaleConfig.scaleVolumes : { for i in range(v.count) : "${k}-vol${i}" => v }]...)
  protoNodes       = var.protocolsConfig.enabled ? [for k in range(var.protocolsConfig.protoNodeCount) : "${var.namePrefix}-proto${k}"] : []
  mgmtIP           = ibm_is_instance.mgmtNode[local.mgmtNodes[0]].primary_network_interface[0].primary_ip[0].address
}

data "ibm_is_ssh_key" "sycomp-keys" {
  for_each = toset(var.admin.rootsshKeyNames)
  name     = each.key
}

data "ibm_is_image" "mgmtImage" {
  name = var.mgmtConfig.mgmtImage
}

data "ibm_is_image" "scaleImage" {
  name = var.scaleConfig.scaleImage
}

data "ibm_is_image" "clientImage" {
  name = var.clientConfig.clientImage
}


resource "ibm_is_instance_template" "mgmtNode" {
  name           = "${var.namePrefix}-mgmt"
  image          = data.ibm_is_image.scaleImage.id
  profile        = var.mgmtConfig.mgmtMachineType
  keys           = local.sshKeyIds
  vpc            = module.sycomp-vpc.vpc_id
  resource_group = ibm_resource_group.sycomp-mgmt-rg.id
  zone           = var.zone
  primary_network_interface {
    name            = "eth0"
    subnet          = local.primarySubnet
    security_groups = [ibm_is_security_group.internal-sycomp.id, ibm_is_security_group.mgmt-sycomp.id]
  }
}

resource "ibm_is_instance" "mgmtNode" {
  for_each          = toset(local.mgmtNodes)
  name              = each.key
  instance_template = ibm_is_instance_template.mgmtNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = ""
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags = concat(var.userDefinedTags, ["${var.namePrefix}-mgmt"])
}

resource "ibm_is_floating_ip" "mgmtNodePublicIP" {
  for_each       = var.mgmtConfig.mgmtPublicIp ? toset(local.mgmtNodes) : []
  name           = "${each.key}-ip"
  resource_group = ibm_resource_group.sycomp-mgmt-rg.id
  target         = ibm_is_instance.mgmtNode[each.key].primary_network_interface[0].id
  tags           = var.userDefinedTags
}

resource "ibm_is_instance_template" "clientNode" {
  name           = "${var.namePrefix}-client"
  image          = data.ibm_is_image.clientImage.id
  profile        = var.clientConfig.clientMachineType
  keys           = local.sshKeyIds
  vpc            = module.sycomp-vpc.vpc_id
  resource_group = ibm_resource_group.sycomp-client-rg.id
  zone           = var.zone
  primary_network_interface {
    name            = "eth0"
    subnet          = local.primarySubnet
    security_groups = [ibm_is_security_group.internal-sycomp.id]
  }
  dynamic "network_interfaces" {
    for_each = toset(local.secondarySubnets)
    content {
      subnet          = network_interfaces.key
      security_groups = [ibm_is_security_group.internal-sycomp.id]
    }

  }
}

resource "ibm_is_instance" "clientNode" {
  for_each          = toset(local.clientNodes)
  name              = each.key
  instance_template = ibm_is_instance_template.clientNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = local.mgmtIP
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags       = concat(var.userDefinedTags, ["${var.namePrefix}-client"])
  depends_on = [ibm_is_instance.mgmtNode]
}

resource "ibm_is_instance_template" "scaleNode" {
  name           = "${var.namePrefix}-scale"
  image          = data.ibm_is_image.scaleImage.id
  profile        = var.scaleConfig.scaleMachineType
  keys           = local.sshKeyIds
  vpc            = module.sycomp-vpc.vpc_id
  resource_group = ibm_resource_group.sycomp-scale-rg.id
  zone           = var.zone
  primary_network_interface {
    name            = "eth0"
    subnet          = local.primarySubnet
    security_groups = [ibm_is_security_group.internal-sycomp.id]
  }
  dynamic "network_interfaces" {
    for_each = toset(concat([local.backendSubnet], local.secondarySubnets))
    content {
      subnet          = network_interfaces.key
      security_groups = [ibm_is_security_group.internal-sycomp.id]
    }

  }
}

resource "ibm_is_instance" "scaleNode" {
  for_each          = toset(local.scaleNodes)
  name              = each.key
  instance_template = ibm_is_instance_template.scaleNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = local.mgmtIP
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags       = concat(var.userDefinedTags, ["${var.namePrefix}-scale"])
  depends_on = [ibm_is_instance.mgmtNode]
}

module "scaleVolumes" {
  source            = "../terraform-ibm/volume"
  for_each          = local.scaleVolumes != {} ? toset(local.scaleNodes) : []
  name              = each.key
  zone              = var.zone
  resource_group_id = ibm_resource_group.sycomp-scale-rg.id
  volumes           = local.scaleVolumes
  instance_id       = ibm_is_instance.scaleNode[each.key].id
  tags              = concat(var.userDefinedTags, ["${var.namePrefix}-scalevol"])
}


resource "ibm_is_instance_template" "protoNode" {
  name           = "${var.namePrefix}-proto"
  image          = data.ibm_is_image.scaleImage.id
  profile        = var.protocolsConfig.protoMachineType
  keys           = local.sshKeyIds
  vpc            = module.sycomp-vpc.vpc_id
  resource_group = ibm_resource_group.sycomp-proto-rg.id
  zone           = var.zone
  primary_network_interface {
    name            = "eth0"
    subnet          = local.primarySubnet
    security_groups = [ibm_is_security_group.internal-sycomp.id]
  }
  dynamic "network_interfaces" {
    for_each = toset(concat([local.backendSubnet], local.secondarySubnets))
    content {
      subnet          = network_interfaces.key
      security_groups = [ibm_is_security_group.internal-sycomp.id]
    }

  }
}

resource "ibm_is_instance" "protoNode" {
  for_each          = toset(local.protoNodes)
  name              = each.key
  instance_template = ibm_is_instance_template.protoNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = local.mgmtIP
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags       = concat(var.userDefinedTags, ["${var.namePrefix}-proto"])
  depends_on = [ibm_is_instance.mgmtNode]
}

resource "ibm_is_subnet_reserved_ip" "sharedIP" {
  for_each = toset(local.protoNodes)
  subnet   = local.primarySubnet
  name     = each.key
}

locals {
  sycompData = templatefile("${path.module}/templates/sycomp_data.tftpl", {
    timestamp    = formatdate("EEE MMM DD hh:mm:ss ZZZ YYYY", timestamp()),
    prefix       = var.namePrefix,
    gui_servers  = length(local.mgmtNodes),
    nsd_servers  = length(local.scaleNodes),
    ces_servers  = length(local.protoNodes),
    clients      = length(local.clientNodes),
    gui_nodes    = { for index, v in values(ibm_is_instance.mgmtNode) : index => v },
    nsd_nodes    = { for index, v in values(ibm_is_instance.scaleNode) : index => v },
    ces_nodes    = { for index, v in values(ibm_is_instance.protoNode) : index => v },
    client_nodes = { for index, v in values(ibm_is_instance.clientNode) : index => v },
    nsd_disks = { for k, v in module.scaleVolumes : k => { for index, j in values(v.volume_attachments) : index => {
      name   = j.volume_name,
      serial = substr(j.device, 0, 20),
      size   = j.capacity
    } } }
    public_ips        = ibm_is_floating_ip.mgmtNodePublicIP
    shared_ips        = ibm_is_subnet_reserved_ip.sharedIP
    shared_ips_prefix = split("/", var.networkConfig.subnets.primarySubnet.addressPrefix)[1]
    quorum_count      = length(local.scaleNodes) >= 5 ? 5 : length(local.scaleNodes) >= 3 ? 3 : 1
    metadata_count    = length(local.scaleNodes)
    manager_count     = length(local.scaleNodes)
  })
}

resource "null_resource" "cluster_dev" {
  count = length(fileset("${path.module}/../../files/opt/sycomp/", "**")) > 1 ? 1 : 0
  connection {
    host        = ibm_is_floating_ip.mgmtNodePublicIP[local.mgmtNodes[0]].address
    type        = "ssh"
    user        = var.admin.sshUserName
    private_key = local.sshPrivateKey
    script_path = "/home/${var.admin.sshUserName}/terraform_%RAND%.sh"
  }
  provisioner "file" {
    source      = "${path.module}/../../files/opt/sycomp"
    destination = "/home/${var.admin.sshUserName}/"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mv -f /home/${var.admin.sshUserName}/sycomp/* /opt/sycomp/",
    ]
    on_failure = fail
  }
  depends_on = [
    ibm_is_instance.mgmtNode
  ]
}

resource "null_resource" "cluster_config" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", [for node in ibm_is_instance.mgmtNode : node.id])}"
    host                 = ibm_is_floating_ip.mgmtNodePublicIP[local.mgmtNodes[0]].address
    user                 = var.admin.sshUserName
  }

  connection {
    host        = self.triggers.host
    type        = "ssh"
    user        = self.triggers.user #var.admin.sshUserName
    private_key = local.sshPrivateKey
    script_path = "/home/${var.admin.sshUserName}/terraform_%RAND%.sh"
  }

  provisioner "file" {
    content     = local.sycompData
    destination = "/home/${var.admin.sshUserName}/sycomp-storage.json"
  }

  provisioner "file" {
    content     = local.sshPrivateKey
    destination = "/home/${var.admin.sshUserName}/.ssh/id_rsa"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv -f /home/${var.admin.sshUserName}/sycomp-storage.json /opt/sycomp/etc/sycomp-storage.json",
      "sudo sh -c 'echo ${local.mgmtIP} > /opt/sycomp/data/masterNode'",
      "chmod 600 /home/${var.admin.sshUserName}/.ssh/id_rsa",
      "sudo chmod 750 /opt/sycomp/bin/scalesetup",
      "sudo /opt/sycomp/bin/scalesetup -d"
    ]
    on_failure = fail
  }
  depends_on = [
    ibm_is_instance.mgmtNode,
    null_resource.cluster_dev
  ]
}
