variable "namePrefix" {
  type = string
}

variable "zone" {
  type = string
}

variable "networkConfig" {
  type = any
}

variable "userDefinedTags" {
  type = list(string)
}
variable "scaleConfig" {
  description = "Scale cluster's target configuration"
  type        = any
}

variable "scaleBareMetalConfig" {
  description = "Scale cluster's target configuration"
  type        = any
}


variable "protocolsConfig" {
  description = "Scale's data access protocols configuration data"
  type        = any
}

variable "clientConfig" {
  description = "Base configuration for client nodes"
  type        = any
}

variable "mgmtConfig" {
  description = "Base configuration for mgmt nodes"
  type        = any
}

variable "admin" {
  description = "SSH configuration for admin"
  type        = any
}
