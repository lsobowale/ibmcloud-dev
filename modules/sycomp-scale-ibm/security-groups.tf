#All nodes sg
resource "ibm_is_security_group" "internal-sycomp" {
  name           = "${var.namePrefix}-internal-sg"
  vpc            = module.sycomp-vpc.vpc_id
  resource_group = ibm_resource_group.sycomp-rg.id
}

resource "ibm_is_security_group_rule" "sycomp-all-outbound" {
  group     = ibm_is_security_group.internal-sycomp.id
  direction = "outbound"
}

resource "ibm_is_security_group_rule" "sycomp-all" {
  group     = ibm_is_security_group.internal-sycomp.id
  direction = "inbound"
  remote    = ibm_is_security_group.internal-sycomp.id
}

#mgmt nodes sg
resource "ibm_is_security_group" "mgmt-sycomp" {
  name           = "${var.namePrefix}-mgmt-sg"
  vpc            = module.sycomp-vpc.vpc_id
  resource_group = ibm_resource_group.sycomp-rg.id
}

resource "ibm_is_security_group_rule" "sycomp-icmp" {
  for_each  = toset(var.mgmtConfig.sshSourceIPs)
  group     = ibm_is_security_group.mgmt-sycomp.id
  direction = "inbound"
  remote    = each.key
  icmp {}
}

resource "ibm_is_security_group_rule" "sycomp-ssh" {
  for_each  = toset(var.mgmtConfig.sshSourceIPs)
  group     = ibm_is_security_group.mgmt-sycomp.id
  direction = "inbound"
  remote    = each.key
  tcp {
    port_min = 22
    port_max = 22
  }
}

resource "ibm_is_security_group_rule" "sycomp-https" {
  for_each  = toset(var.mgmtConfig.httpsSourceIPs)
  group     = ibm_is_security_group.mgmt-sycomp.id
  direction = "inbound"
  remote    = each.key
  tcp {
    port_min = 443
    port_max = 443
  }
}
