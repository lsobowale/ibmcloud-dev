locals {
  addressPrefixes = concat(
    [{
      name     = "${var.namePrefix}-${var.networkConfig.subnets.primarySubnet.name}"
      zone     = var.zone
      ip_range = var.networkConfig.subnets.primarySubnet.addressPrefix
      },
      {
        name     = "${var.namePrefix}-backend-subnet"
        zone     = var.zone
        ip_range = "172.31.252.0/22"
    }],
    [for k in var.networkConfig.subnets.secondarySubnets : {
      name     = "${var.namePrefix}-${k.name}"
      zone     = var.zone
      ip_range = k.addressPrefix
  }])
  subnets = { for subs in local.addressPrefixes : subs.name => {
    zone = subs.zone,
  ip_range = subs.ip_range } }

  zones = distinct(local.addressPrefixes[*].zone)
}

module "sycomp-vpc" {
  source                 = "../terraform-ibm/vpc"
  create_vpc             = var.networkConfig.createVpc
  vpc_name               = "${var.namePrefix}-${var.networkConfig.vpcName}"
  vpc                    = var.networkConfig.vpcName
  resource_group_id      = ibm_resource_group.sycomp-rg.id
  default_address_prefix = "manual"
  address_prefixes       = local.addressPrefixes
  vpc_tags               = var.userDefinedTags
}

resource "ibm_is_public_gateway" "pgw" {
  for_each       = toset(local.zones)
  name           = "${var.namePrefix}-${each.key}-pgw"
  resource_group = ibm_resource_group.sycomp-rg.id
  vpc            = module.sycomp-vpc.vpc_id
  zone           = each.key
  tags           = var.userDefinedTags
}

resource "ibm_is_subnet" "subnet" {
  for_each        = local.subnets
  name            = each.key
  resource_group  = ibm_resource_group.sycomp-rg.id
  vpc             = module.sycomp-vpc.vpc_id
  zone            = each.value.zone
  ipv4_cidr_block = each.value.ip_range
  public_gateway  = ibm_is_public_gateway.pgw[each.value.zone].id
  depends_on      = [module.sycomp-vpc]
}
