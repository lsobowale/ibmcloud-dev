#Resource group for all resources
resource "ibm_resource_group" "sycomp-rg" {
  name = "${var.namePrefix}-sycomp-rg"
}

#Resource group for mgmt nodes
resource "ibm_resource_group" "sycomp-mgmt-rg" {
  name = "${var.namePrefix}-sycomp-mgmt-rg"
}

#Resource group for client nodes
resource "ibm_resource_group" "sycomp-client-rg" {
  name = "${var.namePrefix}-sycomp-client-rg"
}

#Resource group for scale nodes
resource "ibm_resource_group" "sycomp-scale-rg" {
  name = "${var.namePrefix}-sycomp-scale-rg"
}

#Resource group for protocol nodes
resource "ibm_resource_group" "sycomp-proto-rg" {
  name = "${var.namePrefix}-sycomp-proto-rg"
}
