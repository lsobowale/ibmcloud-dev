variable "namePrefix" {
  type = string
}

variable "zone" {
  type = string
}

variable "userDefinedTags" {
  type = list(string)
}

variable "admin" {
  description = "SSH configuration for admin"
  type        = any
}

variable "addScaleNodes" {
  type    = number
  default = 0
}

variable "addClientNodes" {
  type    = number
  default = 0
}

variable "addProtoNodes" {
  type    = number
  default = 0
}

variable "addDisks" {
  type    = any
  default = {}
}
