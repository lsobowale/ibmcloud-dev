data "ibm_resource_group" "mgmt-rg" {
  name = "${var.namePrefix}-sycomp-mgmt-rg"
}

data "ibm_is_instances" "mgmtNode" {
  resource_group = data.ibm_resource_group.mgmt-rg.id
}

data "ibm_resource_group" "client-rg" {
  name = "${var.namePrefix}-sycomp-client-rg"
}

data "ibm_is_instances" "clientNode" {
  resource_group = data.ibm_resource_group.client-rg.id
}

data "ibm_is_instance_template" "clientNode" {
  name = "${var.namePrefix}-client"
}

data "ibm_resource_group" "scale-rg" {
  name = "${var.namePrefix}-sycomp-scale-rg"
}

data "ibm_is_instances" "scaleNode" {
  resource_group = data.ibm_resource_group.scale-rg.id
}

data "ibm_is_instance_template" "scaleNode" {
  name = "${var.namePrefix}-scale"
}

data "ibm_resource_group" "proto-rg" {
  name = "${var.namePrefix}-sycomp-proto-rg"
}

data "ibm_is_instances" "protoNode" {
  resource_group = data.ibm_resource_group.proto-rg.id
}

data "ibm_is_instance_template" "protoNode" {
  name = "${var.namePrefix}-proto"
}

data "ibm_is_volume" "scaleVolume" {
  for_each = toset(local.currentScaleVolumes)
  name     = each.key
}

data "ibm_is_instance_volume_attachments" "scaleVolumes" {
  for_each = local.currentScaleNodes
  instance = each.value.id
}

data "ibm_is_subnet" "subnet" {
  identifier = local.primarySubnet
}

data "ibm_is_subnet_reserved_ips" "sharedip" {
  subnet = local.primarySubnet
}

data "ibm_is_floating_ip" "mgmtNodePublicIP" {
  for_each = local.currentMgmtNodes
  name     = "${each.key}-ip"
}
