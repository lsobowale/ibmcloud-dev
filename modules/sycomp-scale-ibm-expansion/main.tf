locals {
  sshPublicKey        = file(var.admin.sshUserPublicKey)
  sshPrivateKey       = file(var.admin.sshUserPrivateKey)
  currentScaleNodes   = { for k in data.ibm_is_instances.scaleNode.instances : k.name => k }
  addScaleNodes       = [for k in range(var.addScaleNodes) : "${var.namePrefix}-scale${k + length(local.currentScaleNodes)}"]
  currentScaleVolumes = [for k in data.ibm_is_instances.scaleNode.instances[0].volume_attachments : k.volume_name if endswith(k.volume_name, "boot-volume") != true]
  currentScaleStorage = { for k, v in data.ibm_is_volume.scaleVolume : trimprefix(k, "${data.ibm_is_instances.scaleNode.instances[0].name}-") => {
    profile  = v.profile
    sizeInGB = v.capacity
  } }
  currentStorageCount = {
    metadata-storage = length({ for k, v in local.currentScaleStorage : k => v if startswith(k, "metadata-storage") })
    t1-storage       = length({ for k, v in local.currentScaleStorage : k => v if startswith(k, "t1-storage") })
    t2-storage       = length({ for k, v in local.currentScaleStorage : k => v if startswith(k, "t2-storage") })
  }
  addVolume          = merge([for k, v in var.addDisks : { for i in range(v.count) : "${k}-vol${i + local.currentStorageCount[k]}" => v }]...)
  addScaleNodeVolume = merge(local.currentScaleStorage, local.addVolume)
  currentClientNodes = { for k in data.ibm_is_instances.clientNode.instances : k.name => k }
  addClientNodes     = [for k in range(var.addClientNodes) : "${var.namePrefix}-client${k + length(local.currentClientNodes)}"]
  currentProtoNodes  = { for k in data.ibm_is_instances.protoNode.instances : k.name => k }
  addProtoNodes      = [for k in range(var.addProtoNodes) : "${var.namePrefix}-proto${k + length(local.currentProtoNodes)}"]
  currentMgmtNodes   = { for k in data.ibm_is_instances.mgmtNode.instances : k.name => k }
  primarySubnet      = data.ibm_is_instances.scaleNode.instances[0].primary_network_interface[0].subnet
  currentSharedIps   = { for k in data.ibm_is_subnet_reserved_ips.sharedip.reserved_ips : k.name => k if contains(keys(local.currentProtoNodes), k.name) }
  pruneCurrNsdDisks  = { for k, v in data.ibm_is_instance_volume_attachments.scaleVolumes : k => [for i in v.volume_attachments : i if i.type != "boot"] }
  addNsdDisks        = { for k, v in module.addVolume : k => [for i in v.volume_attachments : i] }
  addCurrentNsdDisks = { for k, v in local.pruneCurrNsdDisks : k => concat(v, try(local.addNsdDisks[k], [])) }
  currentNsdDisks = { for k, v in local.addCurrentNsdDisks : k => { for index, j in v : index => {
    name   = j.volume_name,
    serial = substr(j.device, 0, 20),
    size   = try(j.capacity, try(data.ibm_is_volume.scaleVolume[j.volume_name].capacity, 0))
  } if j.type == "data" } }
}

module "addVolume" {
  source            = "../terraform-ibm/volume"
  for_each          = local.addVolume != {} ? local.currentScaleNodes : {}
  name              = each.key
  zone              = var.zone
  resource_group_id = data.ibm_resource_group.scale-rg.id
  volumes           = local.addVolume
  instance_id       = each.value.id
  tags              = concat(var.userDefinedTags, ["${var.namePrefix}-scalevol"])
}

resource "ibm_is_instance" "addScaleNode" {
  for_each          = toset(local.addScaleNodes)
  name              = each.key
  instance_template = data.ibm_is_instance_template.scaleNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = data.ibm_is_instances.mgmtNode.instances[0].primary_network_interface[0].primary_ip[0].address
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags = concat(var.userDefinedTags, ["${var.namePrefix}-scalevm"])
}

module "addScaleNodeVolume" {
  source            = "../terraform-ibm/volume"
  for_each          = local.addScaleNodeVolume != {} ? toset(local.addScaleNodes) : []
  name              = each.key
  zone              = var.zone
  resource_group_id = data.ibm_resource_group.scale-rg.id
  volumes           = local.addScaleNodeVolume
  instance_id       = ibm_is_instance.addScaleNode[each.key].id
  tags              = concat(var.userDefinedTags, ["${var.namePrefix}-scalevol"])
}

resource "ibm_is_instance" "addClientNode" {
  for_each          = toset(local.addClientNodes)
  name              = each.key
  instance_template = data.ibm_is_instance_template.clientNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = data.ibm_is_instances.mgmtNode.instances[0].primary_network_interface[0].primary_ip[0].address
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags = concat(var.userDefinedTags, ["${var.namePrefix}-clientvm"])
}

resource "ibm_is_instance" "addProtoNode" {
  for_each          = toset(local.addProtoNodes)
  name              = each.key
  instance_template = data.ibm_is_instance_template.protoNode.id
  boot_volume {
    name               = "${each.key}-boot-volume"
    auto_delete_volume = true
    size               = 250
  }
  user_data = templatefile("${path.module}/templates/user_data.tftpl", {
    masterNodeIP = data.ibm_is_instances.mgmtNode.instances[0].primary_network_interface[0].primary_ip[0].address
    sshUserName  = var.admin.sshUserName
    sshPublicKey = local.sshPublicKey
  })
  tags = concat(var.userDefinedTags, ["${var.namePrefix}-protovm"])
}
resource "ibm_is_subnet_reserved_ip" "sharedIP" {
  for_each = toset(local.addProtoNodes)
  subnet   = local.primarySubnet
  name     = each.key
}

locals {
  totalScale  = length(local.currentScaleNodes) + length(resource.ibm_is_instance.addScaleNode)
  totalProto  = length(local.currentProtoNodes) + length(resource.ibm_is_instance.addProtoNode)
  totalClient = length(local.currentClientNodes) + length(resource.ibm_is_instance.addClientNode)
  sycompData = templatefile("${path.module}/templates/sycomp_data.tftpl", {
    timestamp    = formatdate("EEE MMM DD hh:mm:ss ZZZ YYYY", timestamp()),
    prefix       = var.namePrefix,
    gui_servers  = length(local.currentMgmtNodes),
    nsd_servers  = local.totalScale,
    ces_servers  = local.totalProto,
    clients      = local.totalClient,
    gui_nodes    = { for index, v in values(local.currentMgmtNodes) : index => v },
    nsd_nodes    = { for index, v in values(merge(local.currentScaleNodes, ibm_is_instance.addScaleNode)) : index => v },
    ces_nodes    = { for index, v in values(merge(local.currentProtoNodes, ibm_is_instance.addProtoNode)) : index => v },
    client_nodes = { for index, v in values(merge(local.currentClientNodes, ibm_is_instance.addClientNode)) : index => v },
    nsd_disks = merge(local.currentNsdDisks, { for k, v in module.addScaleNodeVolume : k => { for index, j in values(v.volume_attachments) : index => {
      name   = j.volume_name,
      serial = substr(j.device, 0, 20),
      size   = j.capacity
    } } })
    public_ips        = data.ibm_is_floating_ip.mgmtNodePublicIP
    shared_ips        = merge(local.currentSharedIps, ibm_is_subnet_reserved_ip.sharedIP)
    shared_ips_prefix = split("/", data.ibm_is_subnet.subnet.ipv4_cidr_block)[1]
    quorum_count      = local.totalScale >= 5 ? 5 : local.totalScale >= 3 ? 3 : 1
    metadata_count    = local.totalScale
    manager_count     = local.totalScale
  })
}

resource "null_resource" "cluster_config" {
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_instance_ids = "${join(",", [for node in local.currentMgmtNodes : node.id])}"
    host                 = data.ibm_is_floating_ip.mgmtNodePublicIP["${var.namePrefix}-mgmt0"].address
    user                 = var.admin.sshUserName
  }

  connection {
    host        = self.triggers.host
    type        = "ssh"
    user        = self.triggers.user #var.admin.sshUserName
    private_key = local.sshPrivateKey
    script_path = "/home/${var.admin.sshUserName}/terraform_%RAND%.sh"
  }

  provisioner "file" {
    content     = local.sycompData
    destination = "/home/${var.admin.sshUserName}/sycomp-storage.json"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv -f /home/${var.admin.sshUserName}/sycomp-storage.json /opt/sycomp/etc/sycomp-storage.json",
      # "sudo chmod 750 /opt/sycomp/bin/scalesetup",      
      # "sudo /opt/sycomp/bin/scalesetup -d"
    ]
    on_failure = fail
  }
}
