######################################################################################################
# Copyright © 2020-2023 Sycomp, A Technology Company, Inc and EnlightenStor Consulting LLC.
# All Rights Reserved
#
# This variable file encapsulates customizable paramteres for Sycomp's IBM Spectrum Scale 
# offering in public cloud environments. 
#
# Customers must provide non-default values that align with supported norms to deploy a cluster that can 
# be supported. 
#
########################################################################################################

variable "cloud" {
  description = "Cloud platform configuration"
  type        = any
}

// namePrefix is required, must be passed. No defaults applied. Name can not contain '-' (mmcrnsd does not accept it)
variable "namePrefix" {
  type        = string
  description = "This string will be used as the prefix for every name. Use only alphanumeric characters."
  nullable    = false

  validation {
    condition     = can(regex("^[0-9a-z]+$", var.namePrefix))
    error_message = "For namePrefix: only a-z, and 0-9 are allowed."
  }
}

variable "deployMode" {
  description = "Deploy mode"
  type        = string
}


variable "userDefinedTags" {
  type    = list(string)
  default = []
}

variable "newDeployment" {
  description = "New deployment"
  type        = any
  default     = {}
}

variable "expansionDeployment" {
  description = "Expansion deployment"
  type        = any
  default     = {}
}

variable "admin" {
  description = "SSH configuration for admin"
  type        = any
  default     = {}
}
