cloud = {
  region = "us-south"   // Region name for deployment
  zone   = "us-south-1" // Zone name for deployment
}

namePrefix = "name-prefix" // namePrefix is required, must be passed. No defaults applied. Name can not contain '-' (mmcrnsd does not accept it)
deployMode = "expansion"   // can only be either "new" or "expansion". 
admin = {
  sshUserName       = "sycompacct"
  sshUserPrivateKey = "~/.ssh/id_rsa"
  sshUserPublicKey  = "~/.ssh/id_rsa.pub"
}
userDefinedTags = [
  "key1:value1",
  "anyTag"
]
#This block is only needed for expansion deployments
expansionDeployment = {
  addScaleNodes  = 1
  addClientNodes = 1
  addProtoNodes  = 0
  addDisks = {
    t1-storage = {
      count    = 1
      sizeInGB = 10           // dataDiskSizeInGB  is used
      profile  = "5iops-tier" //set predefined profile or "custom"(must add iops)
    }
  }
}
