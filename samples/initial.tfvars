cloud = {
  region = "us-south"   // Region name for deployment
  zone   = "us-south-1" // Zone name for deployment
}

namePrefix = "namePrefix" // namePrefix is required, must be passed. No defaults applied. Name can not contain '-' (mmcrnsd does not accept it)
deployMode = "new"        // can only be either "new" or "expansion". 

admin = {
  rootsshKeyNames = ["ssh-key"] //This is a reference to an exisiting ssh-key created in IBM Cloud.
  sshUserName     = "sycompacct"
  sshPrivateKey   = "~/.ssh/id_rsa"
  sshPublicKey    = "~/.ssh/id_rsa.pub"
}

userDefinedTags = [
  "key1:value1",
  "anyTag"
]

#This block is only need for new deployment
newDeployment = {
  mgmtConfig = {
    mgmtNodeCount   = 1
    mgmtMachineType = "cx2-2x4"
    mgmtImage       = "sycomp-redhat-8-6-gpfs-5-1-7-1"
    mgmtPublicIp    = true
    httpsSourceIPs  = ["0.0.0.0/0"]
    sshSourceIPs    = ["0.0.0.0/0"]
  }

  networkConfig = {
    createVpc = true
    vpcName   = "sycomp-vpc"
    subnets = {
      primarySubnet = {
        name          = "primary-subnet"
        addressPrefix = "10.1.0.0/24"
      }
      secondarySubnets = [{
        name          = "secondary-subnet-1"
        addressPrefix = "10.2.0.0/24"
      }]
    }
  }

  scaleConfig = {
    scaleNodeCount   = 1
    scaleMachineType = "cx2-2x4"
    scaleImage       = "sycomp-redhat-8-6-gpfs-5-1-7-1"
    scaleVolumes = {
      metadata-storage = {
        count    = 1
        sizeInGB = 10                //dataDiskSizeInGB  is used
        profile  = "general-purpose" //set predefined profile or "custom"(must add iops)
      }
      t1-storage = {
        count    = 1
        sizeInGB = 10           // dataDiskSizeInGB  is used
        profile  = "5iops-tier" //set predefined profile or "custom"(must add iops)
      }
      t2-storage = {
        count    = 1
        sizeInGB = 10 // dataDiskSizeInGB is used 
        profile  = "10iops-tier"
      }
    }
  }

  clientConfig = {
    clientNodeCount   = 2
    clientImage       = "sycomp-redhat-8-6-gpfs-5-1-7-1"
    clientMachineType = "cx2-2x4"
  }

  // For separate Proto nodes, set enabled to true, protoMachineType to the desired SKU and set protoNodeCount to desired count explicitly.
  // For converged protocol node, set enabled to 'false'.
  protocolsConfig = {
    enabled          = true
    protoNodeCount   = 3
    protoMachineType = "cx2-2x4"
    protocols        = ["NFS"]
  }
}
